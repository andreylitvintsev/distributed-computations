#ifndef LAB0_ADDITIONAL_TOOLS_H
#define LAB0_ADDITIONAL_TOOLS_H

#include <stdio.h>
#include <getopt.h>
#include <ctype.h>
#include <stdlib.h>


extern inline int parseProcessesNumberFromArgs(int argc, char **argv) {
    const char *optString = "p:";

    int c;
    while ((c = getopt(argc, argv, optString)) != -1) {
        if (c == 'p') {
            int result = atoi(optarg);
            return result != 0 ? result : -1;
        }

        if (c == '?') {
            if (optopt == 'p')
                fprintf(stderr, "Option -%c requires an argument.\n", optopt);
            else if (isprint (optopt)) {
                fprintf(stderr, "Unknown option '-%c'.\n", optopt);
            } else {
                fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
            }
            return -1;
        }
    }
    return -1;
}

#endif //LAB0_ADDITIONAL_TOOLS_H
