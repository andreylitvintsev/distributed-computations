#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <time.h>

#include "additional_tools.h"

#include "communication.h"

void sendMessage(TransferDataHelper *transferDataHelper, MessageType messageType) {
    Message message;

    char stringBuffer[255];

    if (transferDataHelper->processIndex != PARENT_ID) {
        // begin STARTED
        message.s_header.s_type = messageType;
        message.s_header.s_magic = MESSAGE_MAGIC;
        message.s_header.s_local_time = (timestamp_t) time(NULL);

        if (messageType == STARTED) {
            sprintf(stringBuffer, log_started_fmt, transferDataHelper->processIndex, getpid(), getppid());
        } else {
            sprintf(stringBuffer, log_done_fmt, transferDataHelper->processIndex);
        }

        strcpy(message.s_payload, stringBuffer);
        message.s_header.s_payload_len = (uint16_t) strlen(stringBuffer);

        printf("%s", stringBuffer);
        write(transferDataHelper->logFileDescriptor, stringBuffer, strlen(stringBuffer));

        if (send_multicast(transferDataHelper, &message) == -1) {
            printf("[Process:%d] Error during multicast sending!\n", transferDataHelper->processIndex);
            exit(EXIT_FAILURE);
        }
    }

    int8_t receivedStartedMessageCount = 0;
    for (local_id i = 1; i < transferDataHelper->processesNumber; ++i) {
        if (i != transferDataHelper->processIndex) {
            if (receive(transferDataHelper, i, &message) == -1) {
                printf("[Process:%d] Error during multicast receiving!\n", transferDataHelper->processIndex);
                exit(EXIT_FAILURE);
            }

            if (message.s_header.s_type == STARTED) {
                ++receivedStartedMessageCount;
            }
        }
    }

    // 'PARENT + CURRENT' or 'CURRENT'
    local_id reduceProcesses = (local_id) (1 + (transferDataHelper->processIndex == PARENT_ID ? PARENT_ID : 1));
    if (receivedStartedMessageCount != transferDataHelper->processesNumber - reduceProcesses) {
        const char * const messageTypeString = messageType == STARTED ? "STARTED" : "DONE";
        printf("[Process:%d] Error! Not all '%s' messages receiving!\n", transferDataHelper->processIndex, messageTypeString);
        exit(EXIT_FAILURE);
    }

    if (messageType == STARTED) {
        sprintf(stringBuffer, log_received_all_started_fmt, transferDataHelper->processIndex);
    } else {
        sprintf(stringBuffer, log_received_all_done_fmt, transferDataHelper->processIndex);
    }
    printf("%s", stringBuffer);
    write(transferDataHelper->logFileDescriptor, stringBuffer, strlen(stringBuffer));
}

void processWork(TransferDataHelper *transferDataHelper, void *additionalData) {
    sendMessage(transferDataHelper, STARTED);
//    sendMessage(transferDataHelper, DONE);
}

int main(int argc, char *argv[]) {
    int8_t processesNumber = (int8_t) parseProcessesNumberFromArgs(argc, argv);
    if (processesNumber == -1 || processesNumber > MAX_PROCESS_ID) {
        printf("Number of processes must be lower than %d\n", MAX_PROCESS_ID);
        exit(EXIT_FAILURE);
    }

    forkAndExecute(processesNumber, processWork, NULL);

    return EXIT_SUCCESS;
}
