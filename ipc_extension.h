#ifndef LAB0_CUSTOM_STRUCTURES_H
#define LAB0_CUSTOM_STRUCTURES_H

#include <malloc.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "ipc.h"

typedef enum {
    READ = 0,
    WRITE = 1,
} PipeDirection;

typedef struct {
    int fileDescriptors[2];
} PipeHolder;

typedef struct {
    int8_t processesNumber;
    local_id processIndex;
    PipeHolder *pipes;
    int8_t pipesNumber;
    int logFileDescriptor;
} TransferDataHelper;

TransferDataHelper *_createTransferDataHelper(int8_t childsNumber);

void _shrinkFileDescriptors(TransferDataHelper *self, local_id currentProcessIndex);

PipeHolder *_getPipeFor(TransferDataHelper *self, local_id fromProcessIndex, local_id toProcessIndex);

void _freeTransferDataHelper(TransferDataHelper *self);

void forkAndExecute(int8_t childsNumber,
                    void (*processWork)(TransferDataHelper *transferDataHelper, void *additionalData),
                    void *additionalData);

//int _getPipeFor(TransferDataHelper *self, local_id fromProcessIndex, local_id toProcessIndex);

//void forkAndExecute(int8_t childsNumber,
//                    void (*parent)(ParentCallbackTime parentCallbackTime, TransferDataHelper *transferDataHelper, void *additionalData),
//                    void (*child)(TransferDataHelper *transferDataHelper, void *additionalData),
//                    void *additionalData);

#endif //LAB0_CUSTOM_STRUCTURES_H
