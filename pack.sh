#!/bin/bash

if [[ -d './pa1' ]]; then
    rm -rf ./pa1
fi

mkdir pa1

cp ./*.c ./pa1
cp ./*.h ./pa1

tar -cvzf pa1.tar.gz pa1

echo 'Packed!'
