#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <fcntl.h>

#include "communication.h"


TransferDataHelper *_createTransferDataHelper(int8_t childsNumber) {
    TransferDataHelper *transferDataHelper = malloc(sizeof(TransferDataHelper));

    transferDataHelper->processesNumber = (int8_t) (childsNumber + 1);

    transferDataHelper->processIndex = -1;

    transferDataHelper->pipesNumber = transferDataHelper->processesNumber * transferDataHelper->processesNumber;

    // alloc connection matrix
    transferDataHelper->pipes = malloc(
            sizeof(PipeHolder) * transferDataHelper->pipesNumber);

    // fill connection matrix
    for (int8_t j = 0; j < transferDataHelper->processesNumber; ++j) {
        for (int8_t i = 0; i < transferDataHelper->processesNumber; ++i) {
            if (i != j) {
                int *fileDescriptors = _getPipeFor(transferDataHelper, i, j)->fileDescriptors;
                pipe(fileDescriptors);
            } else {
                int *reduceFileDescriptors = _getPipeFor(transferDataHelper, i, j)->fileDescriptors;
                reduceFileDescriptors[0] = -1;
                reduceFileDescriptors[1] = -1;
            }
        }
    }

////  check filling
    for (int j = 0; j < transferDataHelper->processesNumber; ++j) {
        for (int i = 0; i < transferDataHelper->processesNumber; ++i) {
            int *fd = transferDataHelper->pipes[j * transferDataHelper->processesNumber + i].fileDescriptors;
            printf("%d_%d    ", fd[0], fd[1]);
        }
        printf("\n");
    }

    transferDataHelper->logFileDescriptor = open("events.log", O_WRONLY | O_CREAT | O_APPEND, 0777);

//  fcntl(transferDataHelper->pipefd[i][0], F_SETFL, O_NONBLOCK);

    return transferDataHelper;
}

// TODO: добавить проверку?
PipeHolder *_getPipeFor(TransferDataHelper *self, local_id fromProcessIndex, local_id toProcessIndex) {
    return &self->pipes[fromProcessIndex * self->processesNumber + toProcessIndex];
}

void _shrinkFileDescriptors(TransferDataHelper *self, local_id currentProcessIndex) { // TODO: переписать
    // close write
    for (int8_t i = 0; i < self->processesNumber; ++i) {
        for (int8_t j = 0; j < self->processesNumber; ++j) {
            if (i != currentProcessIndex) {
                int *fileDescriptors = _getPipeFor(self, i, j)->fileDescriptors;
                if (fileDescriptors[WRITE] != -1) {
                    printf("closing WRITE on %d: %d_%d\n", currentProcessIndex, fileDescriptors[0], fileDescriptors[1]);
                    close(fileDescriptors[WRITE]);
//                    close(fileDescriptors[1]);
                }
            }
        }
    }

    // close read
    for (int8_t i = 0; i < self->processesNumber; ++i) {
        for (int8_t j = 0; j < self->processesNumber; ++j) {
            if (j != currentProcessIndex) {
                int *fileDescriptors = _getPipeFor(self, i, j)->fileDescriptors;
                if (fileDescriptors[READ] != -1) {
                    printf("closing READ on %d: %d_%d\n", currentProcessIndex, fileDescriptors[0], fileDescriptors[1]);
                    close(fileDescriptors[READ]);
                }
            }
        }
    }
}

void _freeTransferDataHelper(TransferDataHelper *self) {
    // TODO: переписать
    for (int8_t j = 0; j < self->processesNumber; ++j) {
        for (int8_t i = 0; i < self->processesNumber; ++i) {
            if (i != j) {
                int *fileDescriptors = _getPipeFor(self, i, j)->fileDescriptors;
                close(fileDescriptors[0]);
                close(fileDescriptors[1]);
            }
        }
    }

    free(self->pipes);

    close(self->logFileDescriptor);

    free(self);
}

int send(void *self, local_id dst, const Message *msg) {
    TransferDataHelper *castedSelf = self;


    int *fileDescriptors = _getPipeFor(castedSelf, castedSelf->processIndex, dst)->fileDescriptors;
    printf("[send %d -> %d : %d]\n", castedSelf->processIndex, dst, fileDescriptors[WRITE]); // TODO: delete!!!

    if (write(fileDescriptors[WRITE], msg, MAX_MESSAGE_LEN) < 0) {
        return -1;
    }

    return 0;
}

int send_multicast(void *self, const Message *msg) {
    TransferDataHelper *castedSelf = self;

    int result = 0;
    for (local_id i = 0; i < castedSelf->processesNumber && result == 0; ++i) {
        if (i != castedSelf->processIndex) {
            result = send(self, i, msg);
        }
    }

    return result;
}

int receive(void *self, local_id from, Message *msg) {
    TransferDataHelper *castedSelf = self;


    int *fileDescriptors = _getPipeFor(castedSelf, from, castedSelf->processIndex)->fileDescriptors;
    printf("[receive %d <- %d : %d]\n", castedSelf->processIndex, from, fileDescriptors[READ]); // TODO: delete!!!

    if (read(fileDescriptors[READ], msg, MAX_MESSAGE_LEN) < 0) {
        return -1;
    }

    return 0;
}

int receive_any(void *self, Message *msg) {
    TransferDataHelper *castedSelf = self;

    int result = 0;
    for (local_id i = 1; i < castedSelf->processesNumber && result == 0; ++i) {
        if (i != castedSelf->processIndex) {
            result = receive(self, i, msg);
        }
    }

    return result;
}

void forkAndExecute(int8_t childsNumber,
                    void (*processWork)(TransferDataHelper *transferDataHelper, void *additionalData),
                    void *additionalData) {
    TransferDataHelper *transferDataHelper = _createTransferDataHelper(childsNumber);

    int parentPID = getpid();

    for (int8_t i = 1; i < childsNumber + 1 && parentPID == getpid(); ++i) {
        int cpid = fork(); // TODO: обработать ошибку
        if (cpid == 0) { // child
            transferDataHelper->processIndex = i;

            _shrinkFileDescriptors(transferDataHelper, i); // TODO: зашринкать

            processWork(transferDataHelper, additionalData);
        }
    }

    if (parentPID == getpid()) {
        transferDataHelper->processIndex = PARENT_ID;

        _shrinkFileDescriptors(transferDataHelper, PARENT_ID); // TODO: зашринкать

        processWork(transferDataHelper, additionalData);

        for (int i = 0; i < childsNumber; ++i) {
            wait(NULL);
        }
    }

    _freeTransferDataHelper(transferDataHelper);
}
